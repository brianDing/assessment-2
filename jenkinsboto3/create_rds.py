import boto3
def create_rds(REGION,DBNAME,DBUSER,DBPASS,AMI,KEY,SGs,NAMETAG):
    response = boto3.session.Session().client('rds', region_name=REGION).create_db_instance(
        DBName=f'{DBNAME}',
        DBInstanceIdentifier='JABS',
        AllocatedStorage=20,
        DBInstanceClass='db.t2.micro',
        Engine='mysql',
        MasterUsername=f'{DBUSER}',
        MasterUserPassword=f'{DBPASS}',
        VpcSecurityGroupIds=SGs,
        DBSubnetGroupName='default-vpc-4bb64132',
        Port=3306,
        MultiAZ=False,
        EngineVersion='8.0.20',
        PubliclyAccessible=True,
        Tags=[
            {
                "Key" : "Name",
                "Value": f"{NAMETAG}"
             },
        ],
        StorageType='standard'
    )

    return response