import boto3
def create_ec2(REGION,AMI,KEY,SGs,NAMETAG):
    # AWS Region
    # AMI code
    # AWS KeyPair
    # AWS SecurityGroups, must be separated by a comma e.g. [sg1, sg2]
    # AWS Nametag
    response = boto3.session.Session().client('ec2',region_name=REGION).run_instances(
    ImageId=f'{AMI}',
    InstanceType='t2.micro',
    KeyName=f'{KEY}',
    MinCount=1,
    MaxCount=1,
    SecurityGroupIds=SGs,
    TagSpecifications=[
        {
            'ResourceType' : 'instance',
            'Tags': [
                {
                    'Key' : 'Name',
                    'Value' : f'{NAMETAG}'
                }
            ]
        },
    ],
    )

    return response


if __name__=='__main__':
    pass