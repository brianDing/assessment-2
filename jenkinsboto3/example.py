import boto3
AWS_PROFILE = "Academy"
AWS_REGION = "eu-west-1"
AWS_KEY = 'BrianTingKey'
AWS_LINUX_AMI = 'ami-0a3f5ff1cb905da33'
#Academy = boto3.session.Session(profile_name = AWS_PROFILE)
#ec2_client = Academy.client('ec2', region_name = AWS_REGION)


response = boto3.session.Session().client('ec2',region_name=AWS_REGION).run_instances(
    ImageId=AWS_LINUX_AMI,
    InstanceType='t2.micro',
    KeyName=AWS_KEY,
    MinCount=1,
    MaxCount=1,
    SecurityGroupIds=[
        'sg-0e333705f96da7f09',
    ],
    TagSpecifications=[
        {
            'ResourceType' : 'instance',
            'Tags': [
                {
                    'Key' : 'Name',
                    'Value' : 'BT-boto3-create-ec2'
                }
            ]
        },
    ],
)

print(response)
