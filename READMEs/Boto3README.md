# Boto3 script ReadMe

## Goals
- Jenkins to run one script called "start_system.py" to start up the system and all the logic on whether to build new systems or to turn existing ones on are done inside the script
- Jenkins to run another script called "stop_system.py" to stop all the running instances in the architecture
  
---
## Structure of modules
- Each python file inside the boto3 script should focus on one type of interaction with AWS
- The start_system and stop_system scripts are wrappers around the modules that actually use the boto3 library to interact with AWS, they themselves do not use boto3
- Create_new_architecture is how we configure each of of the instances with the suitable security groups and amis

---

## start_system.py
- The goal of this script is to check each of the components of the entire architecture (2 load balancers, 2 EC2 servers for PHP and 2 for Pet clinic and the RDS) is off or if the instance exists on AWS.
- If an instance is off, it will turn on the instance instead of creating a new instance on AWS
- If it does not exist, it will create a new instance
- If it is already running then do nothing
- One key assumption is that if one of the system checks fail for a group (Load balancer, PHP EC2, Pet EC2 or RDS), then we would run the create process which will make the entire group (i.e a pair for load balancers and EC2s) instead of making just one

---

## create_architecture.py
- This module creates each of the systems and adds the appropriate security groups by using each of the other modules that interact with AWS through Boto3
- After creating each instance, the instance_id and IP is recorded onto a file on the Jenkins workspace of the main job that calls start_system
- Due to time constraints we did not have time to implement a check that would build a single instance.
- Instead it builds groups of instances (PHP LB or EC2 instances/ Pet LB or EC2 instances)

---
## Other boto3 modules
- By wrapping each Boto3 method call(e.g. create a system, add an ingress, start db, check status etc.) into its own function we could use method parameters to reuse some of our function calls and reduce duplication/ copy + pasting
- Each of the modules and the functions inside were tested first by using my local profile to interact with the Academy VPC before being used on Jenkins and its own AMI user role 