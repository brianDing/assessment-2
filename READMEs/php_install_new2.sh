 #!/bin/bash

ipphp1=$(cat ipphp2)

ssh -o StrictHostKeyChecking=no -i ~/.ssh/JABjenkinsKey.pem ec2-user@ipphp2'

sudo yum install -y httpd
sudo systemctl start httpd
sudo yum -y install git
sudo yum -y install mysql
sudo yum -y install php
sudo yum install php-mbstring -y
sudo yum install php-intl -y
sudo yum install php-pdo -y
sudo yum install php-pdo_mysql -y
sudo systemctl restart httpd
sudo amazon-linux-extras install php7.4 -y
sudo git clone https://bitbucket.org/JangleFett/simple_academy_php_app.git
cd simple_academy_php_app
host=$(cat dbhost)
user=$(cat dbuser)
pass=$(cat dbpass)
sudo sed -i "s,getenv(\"DBHOST\");,\"$host\";," /home/ec2-user/simple_academy_php_app/config.php
sudo sed -i "s,getenv(\"DBUSER\");,\"$user\";," /home/ec2-user/simple_academy_php_app/config.php
sudo sed -i "s,getenv(\"DBPASS\");,\"$pass\";," /home/ec2-user/simple_academy_php_app/config.php
cd simple_academy_php_app
sudo chmod +x install.php
sudo php install.php
sudo cp -r ~/simple_academy_php_app /var/www/html/app
sudo systemctl restart httpd


exit
' 