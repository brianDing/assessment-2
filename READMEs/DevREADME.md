
### What is the automation is doing? 

- Creates and launches six AWS instances
- Create a RDS database
- Extracts the hostname, username, password from the database into a file
- Extracts the IP addresses from each instance and creates a file for each
- Runs provisioning script for PetClinic twice, which will read IPs from petip files
- Runs provisioning script for the PHPapp twice, which will read IPs from phpip files
- Messages on Slack when build is successful, this through an API python script