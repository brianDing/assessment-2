### Deploy Jenkins on an AWS server

Jenkins is used as the automation server for this project. These are the steps to deploy a Jenkins server

- Create a Key Pair for AWS
    Open up Amazon
    Click on Network and Security 
    Click on Key Pairs  
    Enter a name, and put .pem as file format
    Upon creation a private key will also download
- Create a security group
    Click on EC2 on AWS
    Click on Security Groups under Network and Security
    Add your home IP to allow traffic from port 8080 which is HTTPD
    Also add in SSH to allow to your home IP address
- Launch an EC2 instance
    Create a simple Linux free-tier server with the security group and Keypair which has just been created
- Install Jenkins
     SHH into your new instance, and install Jenkins using the commands below
     ``` 
     sudo yum update –y
     sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
    sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
    sudo yum upgrade
    sudo yum install jenkins java-1.8.0-openjdk-devel -y
    sudo systemctl daemon-reload
    sudo systemctl start jenkins
    sudo systemctl status jenkins
    Lastly configure Jenkins

