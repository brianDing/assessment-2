#!/bin/bash

if (( $# > 0 )) # Checks num of args given to stdin (cli args)
then
    # hostname is needed for this
    hostname=$1
else
    echo "WTF! Supply a hostname/IP address" 1>&2
	# Move and merge stdout to stderr
    exit 1
fi

# sudo /etc/init.d/jspaint start
scp -o StrictHostKeyChecking=no -i ~/.ssh/AyeshaShamsiKey.pem haproxyconfig.cfg ubuntu@$hostname:haproxyconfig.cfg
rm -rf *
ssh -o StrictHostKeyChecking=no -i ~/.ssh/AyeshaShamsiKey.pem ubuntu@$hostname '
sudo apt-get install --no-install-recommends software-properties-common
sudo apt update
sudo add-apt-repository -y ppa:vbernat/haproxy-2.2
sudo apt-get install haproxy=2.2.\* -y
sudo systemctl start haproxy
sudo mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy_default.cfg
sudo mv ~/haproxyconfig.cfg /etc/haproxy/haproxy.cfg
sudo systemctl restart haproxy
'