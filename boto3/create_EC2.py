import boto3
import write_to_file as wtf
from botocore.exceptions import ClientError
from config import ec2

def create_ec2(AMI,KEY,SGs,NAMETAG):
    try:
        response = ec2.run_instances(
            ImageId=f'{AMI}',
            InstanceType='t2.micro',
            KeyName=f'{KEY}',
            MinCount=1,
            MaxCount=1,
            SecurityGroupIds=SGs,
            TagSpecifications=[
                {
                    'ResourceType' : 'instance',
                    'Tags': [
                        {
                            'Key' : 'Name',
                            'Value' : f'{NAMETAG}'
                        }
                    ]
                },
            ],
        )
        print(f"Successful creation of {NAMETAG} instance")
        wtf.appendfile('log.txt',f'[SUCCESS] {NAMETAG} instance')
        # log to file 
        return response
    
    except ClientError as e:
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        return e


def record_IP_and_ID(response, ip_file_name, id_file_name, ip_port):
    instance_id = response['Instances'][0]['InstanceId']
    instance_ip = response['Instances'][0]['PrivateIpAddress']
    modified_ip = instance_ip + ":" + str(ip_port)
    wtf.write(ip_file_name, modified_ip)
    wtf.write(id_file_name, instance_id)
    wtf.appendfile('log.txt',f'[SUCCESS] {modified_ip} added to {ip_file_name}')
    wtf.appendfile('log.txt',f'[SUCCESS] {instance_id} added to {id_file_name}')
    return instance_id, instance_ip


    # php1nametag = 'jabs-php1-automation'
    # php1sg = [cfg.SG_PHP_ID]
    # php1 = cec2.create_ec2(REGION=cfg.AWS_REGION,AMI=cfg.AWS_LINUX_AMI,KEY=cfg.AWS_KEY,SGs=php1sg,NAMETAG=php1nametag)
    # php1_InstanceId = php1['Instances'][0]['InstanceId']
    # php1_PrivateIP = php1['Instances'][0]['PrivateIpAddress']
    # ip_to_write = php1_PrivateIP + ":80"
    # wtf.writetofile("ipphp1",ip_to_write)
    # wtf.writetofile("php1instanceid",php1_InstanceId)