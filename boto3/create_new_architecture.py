import config as cfg
import boto3
import create_EC2 as cec2
import create_RDS as crds
from botocore.exceptions import ClientError
#import manage_instance
import time 
import manage_security
import manage_instance
import subprocess
import write_to_file as wtf

def create_RDS_with_SG():
    ## Create RDS with RDS-sg
    db1_response = crds.create_rds(cfg.DB_NAME,[cfg.SG_RDS_ID],'JABS-rds-auto')
    db_record = crds.record(cfg.DB_NAME, cfg.DBHOSTFILE, cfg.DBUSERFILE, cfg.DBPASSFILE)
    wtf.appendfile('log.txt','[SUCCESS] RDS Created')
    return cfg.DB_NAME

def create_and_provision_EC2(mode):
    ## Add to RDS inbound rule source = PHP-sg from 3306 and Pet-sg from 3306
    manage_security.add_inbound(cfg.SG_RDS_ID, [manage_security.create_inbound_rule_sg('tcp', 3306, 3306, cfg.SG_PET_ID, 'Pet clinic SG access'), manage_security.create_inbound_rule_sg('tcp', 3306, 3306, cfg.SG_PHP_ID, 'PHP App SG access')])
    db_record = crds.record(cfg.DB_NAME, cfg.DBHOSTFILE, cfg.DBUSERFILE, cfg.DBPASSFILE)

    if 'php' in mode.lower():
        provfile = cfg.PHP_FILE
        manage_security.add_inbound(cfg.SG_RDS_ID, [manage_security.create_inbound_rule_sg('tcp', 3306, 3306, cfg.SG_LB_ID, 'PHP LB access')])
        ## Add inbound rule from world onto lb security group 
        manage_security.add_inbound(cfg.SG_LB_ID, [manage_security.create_inbound_rule_ip('tcp', 80, 80, '0.0.0.0/0', 'World access')])
        manage_security.add_inbound(cfg.SG_LB_ID, [manage_security.create_inbound_rule_sg('tcp', 22, 22, cfg.SG_JENKINS_ID, 'LB JENKINS access')])

        ## Add to PHP inbound rule source = lb-security group from 80
        manage_security.add_inbound(cfg.SG_PHP_ID, [manage_security.create_inbound_rule_sg('tcp', 80, 80, cfg.SG_LB_ID, 'PHP Load balancer access')])
        manage_security.add_inbound(cfg.SG_PHP_ID, [manage_security.create_inbound_rule_sg('tcp', 22, 22, cfg.SG_JENKINS_ID, 'PHP JENKINS access')])

        ## Create PHP1 and PHP2 with PHP-SG
        instance_nametag = 'jabs-php1-FINAL'
        response1 = cec2.create_ec2(cfg.AWS_LINUX_AMI, cfg.AWS_KEY,[cfg.SG_PHP_ID,cfg.SG_JENKINS_ID], instance_nametag)
        id_1, ip_1 = cec2.record_IP_and_ID(response1, cfg.PHP1_IP, cfg.PHP1_ID, 80)

        instance_nametag = 'jabs-php2-FINAL'
        response2 = cec2.create_ec2(cfg.AWS_LINUX_AMI, cfg.AWS_KEY,[cfg.SG_PHP_ID,cfg.SG_JENKINS_ID], instance_nametag)
        id_2, ip_2 = cec2.record_IP_and_ID(response2, cfg.PHP2_IP, cfg.PHP2_ID, 80)

        ## Create PHP LOADBALANCER with SG-LB
        instance_nametag = 'jabs-php-lb-FINAL'
        response_lb = cec2.create_ec2(cfg.AWS_UBUNTU_AMI, cfg.AWS_KEY,[cfg.SG_LB_ID,cfg.SG_JENKINS_ID], instance_nametag)
        id_lb, ip_lb = cec2.record_IP_and_ID(response_lb, cfg.PHP_LB_IP, cfg.PHP_LB_ID, 80)

        wtf.appendfile('log.txt','[SUCCESS] PHP: 2 webservers and loadbalancer instances created, security group updated')
        ip_sg = ip_lb + '/32'
        manage_security.add_inbound(cfg.SG_PHP_ID, [manage_security.create_inbound_rule_ip('tcp', 80, 80, ip_sg, 'PHP Load balancer access')])

        lbfile = cfg.PHP_LB_FILE
    

    elif 'pet' in mode.lower():
        provfile = cfg.PET_FILE
        manage_security.add_inbound(cfg.SG_RDS_ID, [manage_security.create_inbound_rule_sg('tcp', 3306, 3306, cfg.SG_LB_PET_ID, 'PHP LB access')])

        ## Add inbound rule from world onto lb security group 
        manage_security.add_inbound(cfg.SG_LB_PET_ID, [manage_security.create_inbound_rule_ip('tcp', 80, 80, '0.0.0.0/0', 'World access')])
        manage_security.add_inbound(cfg.SG_LB_PET_ID, [manage_security.create_inbound_rule_ip('tcp', 8080, 8080, '0.0.0.0/0', 'World access')])
        manage_security.add_inbound(cfg.SG_LB_PET_ID, [manage_security.create_inbound_rule_sg('tcp', 22, 22, cfg.SG_JENKINS_ID, 'LB JENKINS access')])

        ## Add to Pet clinic inbound rule source = lb-security group from 8080
        manage_security.add_inbound(cfg.SG_PET_ID, [manage_security.create_inbound_rule_sg('tcp', 8080, 8080, cfg.SG_LB_PET_ID, 'Pet clinic Load balancer access')])
        manage_security.add_inbound(cfg.SG_PET_ID, [manage_security.create_inbound_rule_sg('tcp', 22, 22, cfg.SG_JENKINS_ID, 'PET JENKINS access')])

        ## Create PET1, PET2 with PET-SG
        instance_nametag = 'jabs-pet1-FINAL'
        response1 = cec2.create_ec2(cfg.AWS_LINUX_AMI, cfg.AWS_KEY,[cfg.SG_PET_ID,cfg.SG_JENKINS_ID], instance_nametag)
        id_1, ip_1 = cec2.record_IP_and_ID(response1, cfg.PET1_IP, cfg.PET1_ID, 80)

        instance_nametag = 'jabs-pet2-FINAL'
        response2 = cec2.create_ec2(cfg.AWS_LINUX_AMI, cfg.AWS_KEY,[cfg.SG_PET_ID,cfg.SG_JENKINS_ID], instance_nametag)
        id_2, ip_2 = cec2.record_IP_and_ID(response2, cfg.PET2_IP, cfg.PET2_ID, 80)

        ## Create PET LOADBALANCER with SG-LB
        instance_nametag = 'jabs-pet-lb-FINAL'
        response_lb = cec2.create_ec2(cfg.AWS_UBUNTU_AMI, cfg.AWS_KEY,[cfg.SG_LB_PET_ID,cfg.SG_JENKINS_ID], instance_nametag)
        id_lb, ip_lb = cec2.record_IP_and_ID(response_lb, cfg.PET_LB_IP, cfg.PET_LB_ID, 80)

        wtf.appendfile('log.txt','[SUCCESS] PET: 2 webservers and loadbalancer instances created, security group updated')
        ip_sg = ip_lb + '/32'
        manage_security.add_inbound(cfg.SG_PET_ID, [manage_security.create_inbound_rule_ip('tcp', 8080, 8080, ip_sg, 'Pet clinic Load balancer access')])
        #manage_security.add_inbound(cfg.SG_RDS_ID, [manage_security.create_inbound_rule_ip('tcp', 3306, 3306, ip_sg, 'Pet clinic DB access')])

        lbfile = cfg.PET_LB_FILE

    else:
        print('mode not found')
        
    rdsReady = False
    while rdsReady == False:
        checkRDSstatus = manage_instance.RDS_status(cfg.DB_NAME)
        if checkRDSstatus == 'available':
            rdsReady = True

    

    if rdsReady == True:
        # Provision ec2 - server 1
        if manage_instance.ready_to_provision(id_1) is True:
            print(f'attempting provision of {id_1}')
            wtf.appendfile('log.txt',f'[--INFO--] {mode.upper()} attempting provision of {id_1}')
            run_provision(provfile,ip_1)

        else:
            # Log failing here....
            pass

        # Provision ec2 - server 2
        if manage_instance.ready_to_provision(id_2) is True:
            print(f'attempting provision of {id_2}')
            wtf.appendfile('log.txt',f'[--INFO--] {mode.upper()} attempting provision of {id_2}')
            run_provision(provfile,ip_2)
        else:
            # Log failing here....
            pass

        # Provision Loadbalancer
        if manage_instance.ready_to_provision(id_lb) is True:
            print(f'attempting provision of {id_lb}')
            wtf.appendfile('log.txt',f'[--INFO--] {mode.upper()} attempting provision of {id_lb}')
            run_provision(lbfile,ip_lb)
        else:
            # Log failing here....
            pass
    else:
        print('RDS is not ready')

    return None

    
def run_provision(filename,ip_add):
    subprocess.run(["chmod", "+x", f'{filename}'])
    subprocess.call(f"{filename} {ip_add}", shell=True)
    wtf.appendfile('log.txt',f'[--INFO--] ATTEMPT: {filename} was used to provision ec2 instance @ {ip_add}')
