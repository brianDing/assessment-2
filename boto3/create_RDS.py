import boto3
import write_to_file as wtf
from config import rds, DB_PASS, DB_USER
from botocore.exceptions import ClientError
def create_rds(DBNAME,SGs,NAMETAG):
    try:
        response = rds.create_db_instance(
            DBName=f'{DBNAME}',
            DBInstanceIdentifier=f'{DBNAME}',
            AllocatedStorage=20,
            DBInstanceClass='db.t2.micro',
            Engine='mysql',
            MasterUsername=f'{DB_USER}',
            MasterUserPassword=f'{DB_PASS}',
            VpcSecurityGroupIds=SGs,
            DBSubnetGroupName='default-vpc-4bb64132',
            Port=3306,
            MultiAZ=False,
            EngineVersion='8.0.20',
            PubliclyAccessible=True,
            Tags=[
                {
                    "Key" : "Name",
                    "Value": f"{NAMETAG}"
                 },
            ],
            StorageType='standard'
        )
        wtf.appendfile('log.txt','[SUCCESS] RDS Created!')
        return response

    except ClientError as e:
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        return e


def record(DBNAME, dbhost_file_name, dbuser_file_name, dbpass_file_name):
    dbhost = DBNAME + ".c6jtmnlypkzc.eu-west-1.rds.amazonaws.com"
    wtf.write(dbhost_file_name, dbhost)
    wtf.write(dbuser_file_name, DB_USER)
    wtf.write(dbpass_file_name, DB_PASS)
    wtf.appendfile('log.txt',f'[SUCCESS] {dbhost}: user and password added to {dbuser_file_name}')
    return 'Successfully recorded hostname, username and password'