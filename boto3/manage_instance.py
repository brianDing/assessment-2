import boto3
import time
import write_to_file as wtf
import config
from config import ec2, rds
from botocore.exceptions import ClientError

def turn_on(instance_ids):
    try:
        response = ec2.start_instances(
            InstanceIds = instance_ids,
        )
        
        print(f"Successfully started {instance_ids} instance(s)")
        wtf.appendfile('log.txt',f"[SUCCESS] Successfully started {instance_ids} instance(s)")  
        
    except ClientError as e:
        print(e)
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        
        

def turn_off(instance_ids):
    try:
        response = ec2.stop_instances(
            InstanceIds = instance_ids,
        )
        
        print(f"Successfully stopped {instance_ids} instance(s)")
        wtf.appendfile('log.txt',f"[SUCCESS] Successfully stopped {instance_ids} instance(s)")  
        
    except ClientError as e:
        print(e)
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        
        
        
def instance_status(instance_id):
    try:
        response = ec2.describe_instance_status(
            InstanceIds=[
                instance_id
            ],
        )
        if len(response['InstanceStatuses']) == 0:
            wtf.appendfile('log.txt',f"[SUCCESS] {instance_id} stopped")
            return 'stopped'
        else:
            output = response['InstanceStatuses'][0]['InstanceState']['Name']
            wtf.appendfile('log.txt',f"[SUCCESS] {instance_id} {output}")
            return output

    except ClientError as e:
        print(e)
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        
        
def turn_on_RDS(db_instance_identifier):
        try:
            response = rds.start_db_instance(
                DBInstanceIdentifier = db_instance_identifier
            )
            print(f"Successfully started {db_instance_identifier} RDS")
            wtf.appendfile('log.txt',f"[SUCCESS] Successfully started {db_instance_identifier} RDS")

        except ClientError as e:
            print(e)
            wtf.appendfile('log.txt',f'[FAILURE] {e}')




def turn_off_RDS(db_instance_identifier):
        try:
            response = rds.stop_db_instance(
                DBInstanceIdentifier = db_instance_identifier
            )
            print(f"Successfully stopped {db_instance_identifier} RDS")
            wtf.appendfile('log.txt',f"[SUCCESS] Successfully stopped {db_instance_identifier} RDS\n")

        except ClientError as e:
            print(e)
            wtf.appendfile('log.txt',f'[FAILURE] {e}')



        
def RDS_status(db_instance_identifier):
    try:
        response = rds.describe_db_instances(
            DBInstanceIdentifier = db_instance_identifier
        )
        wtf.appendfile('log.txt','[SUCCESS] RDS Status received')
        return response['DBInstances'][0]['DBInstanceStatus']
        
    except ClientError as e:
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        return "Does not exist"
        
        
        
        
def ready_to_provision(instance_id):
    counter = 0
    while instance_status(instance_id) != 'running' or counter > 50:
        time.sleep(50)
        print(f"{instance_id} is not running yet.")
        counter = counter + 1
    
    if instance_status(instance_id) == 'running':
        print(f'{instance_id} ready for provisioning')
        return True
    
    else:
        print(f"{instance_id} took too long to boot, will not provision it.")
        return False
    
    