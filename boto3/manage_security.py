import sys
import boto3
from botocore.exceptions import ClientError
from config import ec2
import write_to_file as wtf

def add_inbound(sg_id, ip_permissions):
    # Add inbound rule to security group 
    try:
        response = ec2.authorize_security_group_ingress(
            GroupId = sg_id,
            IpPermissions = ip_permissions
        )
        print(f"Successfully added inbound rule to security group: {sg_id}")
        wtf.appendfile('log.txt',f'[SUCCESS] Successfully added inbound rule to security group: {sg_id}')
        
    except ClientError as e:
        wtf.appendfile('log.txt',f'[FAILURE] {e}')
        print(e)
        
        
def create_inbound_rule_ip(ip_protocol, from_port, to_port, ip_source, description):
    return {
        'IpProtocol' : ip_protocol,
        'FromPort' : from_port,
        'ToPort': to_port,
        'IpRanges': [{
            'CidrIp' : ip_source,
            'Description' : description
        }]
    }

def create_inbound_rule_sg(ip_protocol, from_port, to_port, sg_source, description):
    return {
        'IpProtocol' : ip_protocol,
        'FromPort' : from_port,
        'ToPort': to_port,
        'UserIdGroupPairs':[
            {
                'Description': description,
                'GroupId': sg_source
            }
        ]
    }
    