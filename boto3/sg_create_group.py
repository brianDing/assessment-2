import sys
import config
import boto3
from botocore.exceptions import ClientError

Academy = boto3.session.Session(profile_name = config.AWS_PROFILE)
ec2 = Academy.client('ec2', region_name = config.AWS_REGION)

def create_security_group(security_group_name):
    try:
        response = ec2.create_security_group(GroupName = security_group_name , Description='JABS security group', VpcId=config.AWS_VPC)
        # security_group_id = response['GroupId']
        # print(security_group_id)
        return response

    except ClientError as e:
        print(e)
        

# if __name__ == "__init__":
#     if len(sys.argv) > 1:
#         create_security_group(sys.argv[1])