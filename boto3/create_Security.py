import boto3
import config
from botocore.exceptions import ClientError

Academy = boto3.session.Session(profile_name=config.AWS_PROFILE)
ec2 = Academy.client('ec2', region_name="eu-west-1")

# # Look up a security group based on its ID
# try:
#     ec2 = Academy.client('ec2', region_name = config.AWS_REGION)
#     response = ec2.describe_security_groups(GroupIds=['sg-0e333705f96da7f09'])
#     return response

# except ClientError as e:
#     print(e)
    
    
# # Look up a security group based on its name
# try:
#     ec2 = Academy.client('ec2', region_name="eu-west-1")
#     response = ec2.describe_security_groups(GroupNames=['brian-test-mysql-sg'])
#     return response

# except ClientError as e:
#     print(e)
    
    
#  Search profile for VPCs and returns the ID(s)
# response = ec2.describe_vpcs()
# vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')

    

# Add inbound rule to security group 
# try:
#     response = ec2.authorize_security_group_ingress(
#         GroupId = 'sg-00d407e9c481bdaa4',
#         IpPermissions=[
#             {
#                 'IpProtocol' : 'tcp',
#                 'FromPort' : 80,
#                 'ToPort': 80,
#                 'IpRanges': [{
#                     'CidrIp' : '0.0.0.0/0',
#                     'Description' : 'MyIP'
#                 }]
#             }
#         ]
#     )
#     print(response)
    
# except ClientError as e:
#     print(e)
    

# Remove inbound rul from existing Security group 
# try:
#     response = ec2.revoke_security_group_ingress(
#         GroupId = 'sg-00d407e9c481bdaa4',
#         IpPermissions=[
#             {
#                 'IpProtocol' : 'tcp',
#                 'FromPort' : 80,
#                 'ToPort': 80,
#                 'IpRanges': [{
#                     'CidrIp' : '0.0.0.0/0',
#                     'Description' : 'MyIP'
#                 }]
#             }
#         ]
#     )
#     print(response)
    
# except ClientError as e: 
#     print(e)
    
# Add a security group to an instance 
try:
    response = ec2.modify_instance_attribute(InstanceId='i-084f12b4af7eab637', Groups=['sg-0f7461db479535a51'])
    print(response)
    
except ClientError as e: 
    print(e)