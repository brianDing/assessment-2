import create_new_architecture
import manage_instance
import create_RDS as crds
import config


# RDS CHECK
if manage_instance.RDS_status(config.DB_NAME) == "stopped":
    manage_instance.turn_on_RDS(config.DB_NAME)

if manage_instance.RDS_status(config.DB_NAME) == "Does not exist ":
    create_new_architecture.create_RDS_with_SG()

if manage_instance.RDS_status(config.DB_NAME) == "available":
    db_record = crds.record(config.DB_NAME, config.DBHOSTFILE, config.DBUSERFILE, config.DBPASSFILE)



# PHP EC2 CHECK
try: 
    f1 = open(config.PHP1_ID, "r")
    f2 = open(config.PHP2_ID, "r")
    f3 = open(config.PHP_LB_ID, "r")
    
    if manage_instance.instance_status(str(f1.read())) == 'stopped':
        manage_instance.turn_on([str(f1.read())])
        
    if manage_instance.instance_status(str(f2.read())) == 'stopped':
        manage_instance.turn_on([str(f2.read())])
        
    if manage_instance.instance_status(str(f3.read())) == 'stopped':
        manage_instance.turn_on([str(f2.read())])

except FileNotFoundError: 
    create_new_architecture.create_and_provision_EC2('php')
    


# Pet EC2 CHECK
try: 
    f1 = open(config.PET1_ID, "r")
    f2 = open(config.PET2_ID, "r")
    f3 = open(config.PET_LB_ID, "r")
    
    if manage_instance.instance_status(str(f1.read())) == 'stopped':
        manage_instance.turn_on([str(f1.read())])
        
    if manage_instance.instance_status(str(f2.read())) == 'stopped':
        manage_instance.turn_on([str(f2.read())])
        
    if manage_instance.instance_status(str(f3.read())) == 'stopped':
        manage_instance.turn_on([str(f3.read())])


except FileNotFoundError: 
    create_new_architecture.create_and_provision_EC2('pet')



