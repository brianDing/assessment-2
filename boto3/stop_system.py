import create_new_architecture
import manage_instance
from config import DB_NAME, PHP1_ID, PHP2_ID, PHP_LB_ID, PET1_ID, PET2_ID, PET_LB_ID


# stop load balancers + ec2
php_1 = open(PHP1_ID, "r").read()
php_2 = open(PHP2_ID, "r").read()
php_LB = open(PHP_LB_ID, "r").read()
pet_1 = open(PET1_ID, "r").read()
pet_2 = open(PET2_ID, "r").read()
pet_LB = open(PET_LB_ID, "r").read()

manage_instance.turn_off([php_1, php_2, php_LB, pet_1, pet_2, pet_LB])


# rds
manage_instance.turn_off_RDS(DB_NAME)