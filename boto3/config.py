import boto3
AWS_REGION = "eu-west-1"
AWS_PROFILE = 'Academy'
AWS_KEY = 'JABSjenkinsKey'
AWS_LINUX_AMI = 'ami-0a3f5ff1cb905da33'
AWS_UBUNTU_AMI = 'ami-0943382e114f188e8'
AWS_VPC='vpc-4bb64132'
ec2 = boto3.session.Session().client('ec2',region_name=AWS_REGION)
rds = boto3.session.Session().client('rds', region_name=AWS_REGION)

SG_LB_ID  = 'sg-02ff909ee81a0d672'
SG_LB_PET_ID = 'sg-03883e816bacaafdf'
SG_PHP_ID = 'sg-0af4ceb3ec36f2a44'
SG_PET_ID = 'sg-0d220dd602569f157'
SG_RDS_ID = 'sg-0aa40a7e17b5378ad'
SG_JENKINS_ID = 'sg-01733234572795c53'
#SG_JENKINS_ID = '54.74.228.168/32'

DB_NAME = 'jabsdb'
DB_ENDPOINT = DB_NAME + ".c6jtmnlypkzc.eu-west-1.rds.amazonaws.com"
DB_USER = 'admin'
DB_PASS = 'adminpassword'

DBHOSTFILE = 'dbhost'
DBUSERFILE = 'dbuser'
DBPASSFILE = 'dbpass'

PHP1_ID = 'php1instanceid'
PHP1_IP = 'ipphp1'

PHP2_ID = 'php2instanceid'
PHP2_IP = 'ipphp2'

PHP_LB_ID = 'phplbid'
PHP_LB_IP = 'ipphplb'

PET1_ID = 'pet1instanceid'
PET1_IP = 'ippet1'

PET2_ID = 'pet2instanceid'
PET2_IP = 'ippet2'

PET_LB_ID = 'petlbid'
PET_LB_IP = 'ippetlb'

PHP_LB_FILE = '/home/ec2-user/workspace/start_system/PHPapp/loadbalance.sh'
PET_LB_FILE = '/home/ec2-user/workspace/start_system/petclinic/lb_provision_1.sh'
PHP_FILE = '/home/ec2-user/workspace/start_system/PHPapp/php_install_new1.sh'
PET_FILE = '/home/ec2-user/workspace/start_system/petclinic/petclinic_prov1.sh'



