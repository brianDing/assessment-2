#!/bin/bash
if (( $# > 0)) 
then
  hostname=$1 
else
  echo "No, sorry: you must supply a hostname or IP address" 1>&2 
  exit 1 
fi
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/ipphp1 ubuntu@$hostname:
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/ipphp2 ubuntu@$hostname:
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/PHPapp/haproxyconfig.cfg ubuntu@$hostname:
sudo ssh -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem ubuntu@$hostname '
IP1=$(cat ipphp1)
IP2=$(cat ipphp2)
sudo apt-get install --no-install-recommends software-properties-common
sudo apt update
sudo add-apt-repository -y ppa:vbernat/haproxy-2.2
sudo apt-get install haproxy=2.2.\* -y
sudo systemctl start haproxy
sudo mv ~/haproxyconfig.cfg /etc/haproxy/haproxy.cfg
sudo sed -i "s,ipaddress1,$IP1," /etc/haproxy/haproxy.cfg
sudo sed -i "s,ipaddress2,$IP2," /etc/haproxy/haproxy.cfg
sudo systemctl restart haproxy
'