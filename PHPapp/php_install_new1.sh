 #!/bin/bash

if (( $# > 0)) 
then
  hostname=$1 
else
  echo "No, sorry: you must supply a hostname or IP address" 1>&2 
  exit 1 
fi
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/ipphplb ec2-user@$hostname:
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/dbhost ec2-user@$hostname:
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/dbpass ec2-user@$hostname:
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/dbuser ec2-user@$hostname:
sudo ssh -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem ec2-user@$hostname '
host=$(cat dbhost)
user=$(cat dbuser)
pass=$(cat dbpass)
myip=$(cat ipphplb)
sudo yum install -y httpd
sudo systemctl start httpd
sudo yum -y install git
sudo yum -y install mysql
sudo yum -y install php
sudo yum install php-mbstring -y
sudo yum install php-intl -y
sudo yum install php-pdo -y
sudo yum install php-pdo_mysql -y
sudo systemctl restart httpd
sudo amazon-linux-extras install php7.4 -y
sudo git clone https://bitbucket.org/JangleFett/simple_academy_php_app.git
cd simple_academy_php_app
sudo sed -i "s,getenv(\"DBHOST\");,\"$host\";," /home/ec2-user/simple_academy_php_app/config.php
sudo sed -i "s,getenv(\"DBUSER\");,\"$user\";," /home/ec2-user/simple_academy_php_app/config.php
sudo sed -i "s,getenv(\"DBPASS\");,\"$pass\";," /home/ec2-user/simple_academy_php_app/config.php
cd simple_academy_php_app
sudo chmod +x install.php
sudo php install.php
sudo cp -r ~/simple_academy_php_app /var/www/html/app
myip=http://jabs-php.academy.labs.automationlogic.com
sudo sh -c "cat >/var/www/html/index.html << _END_
<h1>PHPapp</h1>
<h2><a href=\"$myip/app/public/\">ENTER</a></h2>
_END_"

sudo systemctl restart httpd


exit
' 