#!/bin/bash

if (( $# > 0 )) # Checks num of args given to stdin (cli args)
then
    # hostname is needed for this adding some
    hostname=$1
else
    echo "WTF! Supply a hostname/IP address" 1>&2
	# Move and merge stdout to stderr
    exit 1
fi

# sudo /etc/init.d/jspaint start
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/petclinic/haproxyconfig_1.1.cfg ubuntu@$hostname:
#rm -rf *
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/ippet1 ubuntu@$hostname:
sudo scp -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem /home/ec2-user/workspace/start_system/ippet2 ubuntu@$hostname:
sudo ssh -o StrictHostKeyChecking=no -i /home/ec2-user/.ssh/JABSjenkinsKey.pem ubuntu@$hostname '
mv haproxyconfig_1.1.cfg haproxyconfig.cfg
sudo apt-get install --no-install-recommends software-properties-common
sudo apt update
sudo add-apt-repository -y ppa:vbernat/haproxy-2.2
sudo apt-get install haproxy=2.2.\* -y
sudo systemctl start haproxy
sudo mv /etc/haproxy/haproxy.cfg /etc/haproxy/haproxy_default.cfg
sudo mv ~/haproxyconfig.cfg /etc/haproxy/haproxy.cfg
IP1=$(cat ippet1)
IP2=$(cat ippet2)
sudo sed -i "s,ipaddress1,$IP1," /etc/haproxy/haproxy.cfg
sudo sed -i "s,ipaddress2,$IP2," /etc/haproxy/haproxy.cfg

sudo systemctl restart haproxy
'